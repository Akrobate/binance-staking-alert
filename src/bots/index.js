'use strict';

const {
    BestDailyStakBot,
} = require('./BestDailyStakBot');

const {
    SurveyStakingBot,
} = require('./SurveyStakingBot');

module.exports = {
    BestDailyStakBot,
    SurveyStakingBot,
};
