'use strict';

const {
    logger,
} = require('../logger');

class AbstractBot {


    /**
     * @returns {AbstractBot}
     */
    constructor() {
        this.bot_status = 'running';
        this.interval = 2000;
    }


    /**
     * @returns {Promise<Void>}
     */
    async run() {
        this.bot_status = 'running';
        while (this.bot_status === 'running') {
            try {
                await this.process();
            } catch (error) {
                logger.log('Process error: ', error);
            }
            await this.sleepAsync(this.interval);
        }
    }

    /**
     * @returns {void}
     */
    stop() {
        this.bot_status = 'stopped';
    }

    /**
     * @returns {Promise<Void>}
     */
    process() {
        throw new Error('SHOULD BE OVERRIDED');
    }

    /**
     * @param {Number} duration
     * @returns {Promise<Void>}
     */
    async sleepAsync(duration) {
        await new Promise((resolve) => setTimeout(resolve, duration));
        return null;
    }

    /**
     * @param {Number} interval
     * @return {Void}
     */
    setBotInterval(interval) {
        this.interval = interval;
    }

}

module.exports = {
    AbstractBot,
};
