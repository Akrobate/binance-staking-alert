'use strict';

const {
    logger,
} = require('../logger');

const {
    BinanceStackingRepository,
    JsonFileRepository,
    TwitterRepository,
} = require('../repositories');

const {
    DifferencesCalculatorService,
    DifferencesReferential,
    TweetTextGeneratorService,
} = require('../services');

class SurveyStakingBot {

    /**
     * @static
     * @returns {SurveyStakingBot}
     */
    static getInstance() {
        if (SurveyStakingBot.instance) {
            return SurveyStakingBot.instance;
        }
        SurveyStakingBot.instance = new SurveyStakingBot(
            BinanceStackingRepository.getInstance(),
            JsonFileRepository.getInstance(),
            TwitterRepository.getInstance(),
            DifferencesCalculatorService.getInstance(),
            TweetTextGeneratorService.getInstance()
        );
        return SurveyStakingBot.instance;
    }


    /**
     * @param {*} binance_stacking_repository
     * @param {*} json_file_repository
     * @param {*} twitter_repository
     * @param {*} differences_calculator_service
     * @param {*} tweet_text_generator_service
     * @returns {SurveyStakingBot}
     */
    constructor(
        binance_stacking_repository,
        json_file_repository,
        twitter_repository,
        differences_calculator_service,
        tweet_text_generator_service
    ) {
        this.binance_stacking_repository = binance_stacking_repository;
        this.json_file_repository = json_file_repository;
        this.twitter_repository = twitter_repository;
        this.differences_calculator_service = differences_calculator_service;
        this.tweet_text_generator_service = tweet_text_generator_service;

        this.bot_status = 'running';
        this.interval = 2000;

        this.differences_to_process_list = [
            DifferencesReferential.NEW_PROJECT,
            DifferencesReferential.NO_MORE_PROJECT,
            DifferencesReferential.SOLD_OUT_FALSE,
            DifferencesReferential.INCREASE_APY,
            DifferencesReferential.DECREASE_APY,
        ];
    }


    /**
     * @returns {Promise<Void>}
     */
    async run() {
        this.bot_status = 'running';
        while (this.bot_status === 'running') {
            try {
                await this.process();
            } catch (error) {
                logger.log('Process error: ', error);
            }
            await this.sleepAsync(this.interval);
        }
    }

    /**
     * @returns {void}
     */
    stop() {
        this.bot_status = 'stopped';
    }

    /**
     * @returns {Promise<Void>}
     */
    async process() {
        const data = await this.getAndUpdateData();
        const differences_list = this.generateDifferencesFromData(data);
        const wanted_differences_list = this.filterOnlyWantedDifferences(differences_list);
        const tweet_list = wanted_differences_list.map(
            (difference) => this.tweet_text_generator_service.generate(difference)
        );
        await this.tweetStatusList(tweet_list);
    }


    /**
     * @returns {Promise<Object>}
     */
    async getAndUpdateData() {
        const fetched_data = await this.binance_stacking_repository.getStakingProducts();
        const saved_data = await this.json_file_repository.getData();
        await this.json_file_repository.saveData(fetched_data);
        return {
            fetched_data,
            saved_data,
        };
    }


    /**
     * @param {Object} data
     * @returns {Object}
     */
    generateDifferencesFromData(data) {
        const {
            fetched_data,
            saved_data,
        } = data;
        this.differences_calculator_service.setFetchedData(fetched_data);
        this.differences_calculator_service.setSavedData(saved_data);
        return this.differences_calculator_service.process();
    }


    /**
     * @param {Array} differences_list
     * @returns {Array}
     */
    filterOnlyWantedDifferences(differences_list) {
        return differences_list.filter(
            (item) => this.differences_to_process_list.includes(item.difference_list[0])
        );
    }


    /**
     * @param {Array<String>} tweet_list
     * @returns {Error|void}
     */
    async tweetStatusList(tweet_list) {
        for (const tweet of tweet_list) {
            try {
                await this.twitter_repository.tweet(tweet);
            } catch (error) {
                logger.log('Twitting error: ');
                logger.log(error);
            }
            await this.sleepAsync(5000);
            logger.log(tweet);
        }
    }


    /**
     * @param {Number} duration
     * @returns {Promise<Void>}
     */
    async sleepAsync(duration) {
        await new Promise((resolve) => setTimeout(resolve, duration));
        return null;
    }

}

module.exports = {
    SurveyStakingBot,
};
