'use strict';

const moment = require('moment');
const {
    std,
    mean,
} = require('mathjs');

const {
    AbstractBot,
} = require('./AbstractBot');

const {
    CryptoCompareRepository,
    BinanceStackingRepository,
    BinanceRepository,
} = require('../repositories');

class BestDailyStakBot extends AbstractBot {


    /**
     * @static
     * @returns {BestDailyStakBot}
     */
    static getInstance() {
        if (BestDailyStakBot.instance) {
            return BestDailyStakBot.instance;
        }
        BestDailyStakBot.instance = new BestDailyStakBot(
            CryptoCompareRepository.getInstance(),
            BinanceRepository.getInstance(),
            BinanceStackingRepository.getInstance()
        );
        return BestDailyStakBot.instance;
    }

    // eslint-disable-next-line require-jsdoc
    constructor(
        crypto_compare_repository,
        binance_repository,
        binance_staking_repository
    ) {
        super();
        this.crypto_compare_repository = crypto_compare_repository;
        this.binance_repository = binance_repository;
        this.binance_staking_repository = binance_staking_repository;

        this.data = {};

        this.coinlist_full_metadata_currencies_list = null;
        this.binance_full_pair_prices = null;
        this.binance_full_staking_data = null;

        this.binance_full_staking_currencies_list = null;
        this.stackin_coins_with_volume = null;
        this.full_aggregated_data = null;

        this.setBotInterval(24 * 3600 * 1000);
    }

    /**
     * Fetch all data for calculation
     * @returns {void}
     */
    async initData() {
        await this.updateCoinListMetadata();
        await this.updatePricesData();
        await this.updateStackingData();
    }


    // eslint-disable-next-line require-jsdoc
    async updateCoinListMetadata() {
        const coinlist_full_metadata = await this.crypto_compare_repository.getCoinList();
        const list = [];
        Object.keys(coinlist_full_metadata).forEach((item) => list.push({
            asset: item,
            total_coins: Number(coinlist_full_metadata[item].TotalCoinsMined),
        }));
        this.coinlist_full_metadata_currencies_list = list
            .filter((item) => item.total_coins)
            .sort((a, b) => b.total_coins - a.total_coins);
        return this.coinlist_full_metadata_currencies_list;
    }

    // eslint-disable-next-line require-jsdoc
    async updatePricesData() {
        this.binance_full_pair_prices = await this.binance_repository.getLatestPrice();
        return this.binance_full_pair_prices;
    }

    // eslint-disable-next-line require-jsdoc
    async updateStackingData() {
        const response = await this.binance_staking_repository
            .getStakingProducts();
        this.binance_full_staking_data = response.data;
        return this.binance_full_staking_data;
    }

    /**
     * @returns {Obejct}
     */
    selectDailyDetectedBestStaking() {
        this.generateDataAggregation();
        const best_project_list = this
            .generateBestProjectListFromAggregatedFullData();
        return this.chooseBestItemFromFullAggregatedData(best_project_list);
    }

    /**
     * Best Daily stack selection process
     * @param {Array} full_aggregated_data
     * @return {Object}
     */
    chooseBestItemFromFullAggregatedData(full_aggregated_data) {
        let best_item = null;
        // Dummy rule for testing
        [
            best_item,
        ] = full_aggregated_data;

        return best_item;
    }


    /**
     * @returns {Void}
     */
    generateDataAggregation() {
        this.binance_full_staking_currencies_list = this
            .binance_full_staking_data
            .map((item) => item.asset);

        this.stackin_coins_with_volume = this.coinlist_full_metadata_currencies_list
            .filter((item) => this.binance_full_staking_currencies_list
                .find((stacking) => stacking === item.asset)
            );

        this.full_aggregated_data = this.stackin_coins_with_volume
            .map((item) => {
                const price_object = this.binance_full_pair_prices
                    .find((price) => price.symbol === `${item.asset}USDT`);

                const staking = this.binance_full_staking_data
                    .find((staking_asset) => staking_asset.asset === `${item.asset}`);

                let usdt_price = null;
                let capitalization = null;

                if (price_object) {
                    usdt_price = Number(price_object.price);
                    capitalization = Number(price_object.price) * item.total_coins;
                }

                const staking_project_list = staking.projects
                    .filter((staking_project) => staking_project.sellOut === false && staking_project.status === 'PURCHASING')
                    .map((staking_project) => ({
                        project_name: staking_project.projectId,
                        duration: Number(staking_project.duration),
                        annual_interest_rate: Number(staking_project.config.annualInterestRate),
                    }));

                return Object.assign(
                    {},
                    item,
                    {
                        usdt_price,
                        capitalization,
                        staking_project_list,
                    }
                );
            })
            .filter((item) => item.capitalization)
            .sort((a, b) => b.capitalization - a.capitalization);
    }


    /**
     * @returns {Array}
     */
    generateBestProjectListFromAggregatedFullData() {
        const project_list_chunked = this.full_aggregated_data
            .map((item) => item.staking_project_list
                .map((proj) => ({
                    asset: item.asset,
                    capitalization: item.capitalization,
                    duration: proj.duration,
                    annual_interest_rate: proj.annual_interest_rate,
                }))
            );
        const project_list = [].concat(...project_list_chunked);

        // @todo: Review architecture should not be here
        // project_list = await this.enrichProjectListWithVarianceScore(project_list);
        return project_list.sort((a, b) => b.annual_interest_rate - a.annual_interest_rate);
    }


    /**
     * Process all selected data and apply variation score
     * for the staking periode
     * @param {Array} project_list
     * @returns {Array}
     */
    async enrichProjectListWithVarianceScore(project_list) {
        for (const element of project_list) {
            element.variance_score = await this.getVariabiltyScoreForCurrency(
                element.asset,
                element.duration
            );
        }
        return project_list;
    }


    /**
     * @param {String} currency
     * @param {String} staking_duration_days
     * @returns {Promise<Object>}
     */
    async getVariabiltyScoreForCurrency(currency, staking_duration_days) {
        const symbol = `${currency}USDT`;
        const params = {
            symbol,
            interval: '1h',
            endTime: moment().format('x'),
            limit: staking_duration_days * 24,
        };

        const OPEN_COLUMN = 1;

        const ohlc_data = await this.binance_repository
            .getCandlestickData(params);

        const open_list = ohlc_data.map((item) => Number(item[OPEN_COLUMN]));
        const standart_deviation = std(open_list);
        const open_mean = mean(open_list);

        return standart_deviation / open_mean;

    }
}

module.exports = {
    BestDailyStakBot,
};
