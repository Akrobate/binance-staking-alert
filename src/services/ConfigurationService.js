
/* eslint-disable global-require */

'use strict';

const fs = require('fs');
class ConfigurationService {

    /* istanbul ignore next */
    /**
     * @static
     * @returns {ConfigurationService}
     */
    static getInstance() {
        if (ConfigurationService.instance) {
            return ConfigurationService.instance;
        }
        ConfigurationService.instance = new ConfigurationService();
        return ConfigurationService.instance;
    }

    // eslint-disable-next-line require-jsdoc
    constructor() {
        this.configuration = null;
    }

    // eslint-disable-next-line require-jsdoc
    static get CONFIGURATION_EXAMPLE_FILE() {
        return 'configuration.example.js';
    }

    // eslint-disable-next-line require-jsdoc
    static get CONFIGURATION_FILE() {
        return 'configuration.js';
    }

    /**
     * @returns {Object}
     */
    lazyLoadConfiguration() {
        if (this.configuration === null) {
            this.configuration = this.loadConfiguration();
        }
        return this.configuration;
    }

    /**
     * @returns {Object}
     */
    loadConfiguration() {
        if (fs.existsSync(`./src/${ConfigurationService.CONFIGURATION_FILE}`)) {
            return this.requireFile(`../${ConfigurationService.CONFIGURATION_FILE}`).configuration;
        }
        return this.requireFile(`../${ConfigurationService.CONFIGURATION_EXAMPLE_FILE}`).configuration;
    }


    /**
     * @param {String} file
     * @returns {Object}
     */
    requireFile(file) {
        return require(file);
    }

}

module.exports = {
    ConfigurationService,
    configuration: ConfigurationService.getInstance().lazyLoadConfiguration(),
};
