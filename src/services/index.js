'use strict';

const {
    DifferencesCalculatorService,
} = require('./DifferencesCalculatorService');

const {
    DifferencesReferential,
} = require('./DifferencesReferential');

const {
    DifferencesRulesService,
} = require('./DifferencesRulesService');

const {
    TweetTextGeneratorService,
} = require('./TweetTextGeneratorService');

const {
    ConfigurationService,
} = require('./ConfigurationService');

module.exports = {
    DifferencesCalculatorService,
    DifferencesReferential,
    DifferencesRulesService,
    TweetTextGeneratorService,
    ConfigurationService,
};
