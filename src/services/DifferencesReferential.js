'use strict';

class DifferencesReferential {

    // eslint-disable-next-line require-jsdoc
    static get NEW_PROJECT() {
        return 'new_project';
    }


    // eslint-disable-next-line require-jsdoc
    static get NO_MORE_PROJECT() {
        return 'no_more_project';
    }


    // eslint-disable-next-line require-jsdoc
    static get SOLD_OUT_TRUE() {
        return 'sold_out_true';
    }


    // eslint-disable-next-line require-jsdoc
    static get SOLD_OUT_FALSE() {
        return 'sold_out_false';
    }


    // eslint-disable-next-line require-jsdoc
    static get INCREASE_APY() {
        return 'increase_apy';
    }


    // eslint-disable-next-line require-jsdoc
    static get DECREASE_APY() {
        return 'decrease_apy';
    }

}

module.exports = {
    DifferencesReferential,
};
