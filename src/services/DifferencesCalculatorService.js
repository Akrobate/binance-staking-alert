'use strict';

const {
    DifferencesRulesService,
} = require('./DifferencesRulesService');

class DifferencesCalculatorService {

    /**
     * @param {JsonFileRepository} differences_rules_service
     * @returns {DifferencesCalculatorService}
     */
    constructor(
        differences_rules_service
    ) {
        this.differences_rules_service = differences_rules_service;

        this.fetched_data = null;
        this.saved_data = null;
    }

    /* istanbul ignore next */
    /**
     * @static
     * @returns {DifferencesCalculatorService}
     */
    static getInstance() {
        if (DifferencesCalculatorService.instance) {
            return DifferencesCalculatorService.instance;
        }
        DifferencesCalculatorService.instance = new DifferencesCalculatorService(
            DifferencesRulesService.getInstance()
        );
        return DifferencesCalculatorService.instance;
    }


    /**
     * @param {Object} data
     * @returns {void}
     */
    setFetchedData(data) {
        this.fetched_data = data;
    }


    /**
     * @param {Object} data
     * @returns {void}
     */
    setSavedData(data) {
        this.saved_data = data;
    }

    /**
     * @param {Array} data
     * @return {Array}
     */
    formatOneLevelData(data) {
        const formated_data = [];
        data.forEach((asset) => {
            formated_data.push(...asset.projects);
        });
        return formated_data;
    }

    /**
     * @return {Array}
     */
    process() {

        const saved_data = this.formatOneLevelData(this.saved_data.data);
        const fetched_data = this.formatOneLevelData(this.fetched_data.data);

        const project_list = this.generateProjectsList(saved_data, fetched_data);
        const old_new_project_list = project_list.map((project_id) => ({
            old_project: saved_data.find((item) => item.projectId === project_id),
            new_project: fetched_data.find((item) => item.projectId === project_id),
        }));

        const all_detections = old_new_project_list.map((old_new_project) => Object
            .assign(
                {},
                old_new_project,
                {
                    difference_list: this.differences_rules_service
                        .detectDifferences(
                            old_new_project.old_project,
                            old_new_project.new_project
                        ),
                }
            )
        );

        return all_detections.filter((item) => item.difference_list.length > 0);
    }


    /**
     * @param {Object} old_data
     * @param {Object} new_data
     * @returns {Array}
     */
    generateProjectsList(old_data, new_data) {
        const project_list = [];

        const add_to_project_list_if_not_includes = (item) => {
            if (!project_list.includes(item.projectId)) {
                project_list.push(item.projectId);
            }
        };

        old_data.forEach(add_to_project_list_if_not_includes);
        new_data.forEach(add_to_project_list_if_not_includes);

        return project_list;
    }
}

module.exports = {
    DifferencesCalculatorService,
};
