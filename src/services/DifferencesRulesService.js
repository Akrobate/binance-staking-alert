'use strict';

const {
    DifferencesReferential,
} = require('./DifferencesReferential');

class DifferencesRulesService {

    /* istanbul ignore next */
    /**
     * @static
     * @returns {DifferencesRulesService}
     */
    static getInstance() {
        if (DifferencesRulesService.instance) {
            return DifferencesRulesService.instance;
        }
        DifferencesRulesService.instance = new DifferencesRulesService();
        return DifferencesRulesService.instance;
    }

    /**
     * @param {Object} old_project
     * @param {Object} new_project
     * @return {Array}
     */
    detectDifferences(old_project, new_project) {
        return [
            this.detectNewProject(old_project, new_project),
            this.detectNoMoreProject(old_project, new_project),
            this.detectSoldOutTrue(old_project, new_project),
            this.detectSoldOutFalse(old_project, new_project),
            this.detectIncreaseDecreaseApy(old_project, new_project),
        ].filter((detection) => detection !== null);
    }


    /**
     * @param {Object} old_project
     * @param {Object} new_project
     * @return {String}
     */
    detectNewProject(old_project, new_project) {
        if (old_project === undefined && new_project !== undefined) {
            return DifferencesReferential.NEW_PROJECT;
        }
        return null;
    }


    /**
     * @param {Object} old_project
     * @param {Object} new_project
     * @return {String}
     */
    detectNoMoreProject(old_project, new_project) {
        if (new_project === undefined && old_project !== undefined) {
            return DifferencesReferential.NO_MORE_PROJECT;
        }
        return null;
    }


    /**
     * @param {Object} old_project
     * @param {Object} new_project
     * @return {String}
     */
    detectSoldOutTrue(old_project, new_project) {
        if (
            new_project !== undefined
            && old_project !== undefined
            && new_project.sellOut === true
            && old_project.sellOut === false
        ) {
            return DifferencesReferential.SOLD_OUT_TRUE;
        }
        return null;
    }


    /**
     * @param {Object} old_project
     * @param {Object} new_project
     * @return {String}
     */
    detectSoldOutFalse(old_project, new_project) {
        if (
            new_project !== undefined
            && old_project !== undefined
            && new_project.sellOut === false
            && old_project.sellOut === true
        ) {
            return DifferencesReferential.SOLD_OUT_FALSE;
        }
        return null;
    }


    /**
     * @param {Object} old_project
     * @param {Object} new_project
     * @return {String}
     */
    detectIncreaseDecreaseApy(old_project, new_project) {
        if (
            new_project !== undefined
            && old_project !== undefined
        ) {

            const old_apy = Number(old_project.config.annualInterestRate);
            const new_apy = Number(new_project.config.annualInterestRate);

            if (old_apy === new_apy) {
                return null;
            }

            if (new_apy > old_apy) {
                return DifferencesReferential.INCREASE_APY;
            }
            return DifferencesReferential.DECREASE_APY;
        }
        return null;
    }


}

module.exports = {
    DifferencesRulesService,
};
