'use strict';

const moment = require('moment');

const {
    DifferencesReferential,
} = require('./DifferencesReferential');

class TweetTextGeneratorService {

    /* istanbul ignore next */
    /**
     * @static
     * @returns {TweetTextGeneratorService}
     */
    static getInstance() {
        if (TweetTextGeneratorService.instance) {
            return TweetTextGeneratorService.instance;
        }
        TweetTextGeneratorService.instance = new TweetTextGeneratorService();
        return TweetTextGeneratorService.instance;
    }


    /**
     * @param {Object} data
     * @returns {String}
     */
    generate(data) {
        const {
            difference_list,
        } = data;

        if (difference_list.includes(DifferencesReferential.NEW_PROJECT)) {
            return this.generateNewProject(data);
        }

        if (difference_list.includes(DifferencesReferential.NO_MORE_PROJECT)) {
            return this.generateNoMoreProject(data);
        }

        if (difference_list.includes(DifferencesReferential.SOLD_OUT_TRUE)) {
            return this.generateSoldOutTrue(data);
        }

        if (difference_list.includes(DifferencesReferential.SOLD_OUT_FALSE)) {
            return this.generateSoldOutFalse(data);
        }

        if (difference_list.includes(DifferencesReferential.INCREASE_APY)) {
            return this.generateIncreaseApy(data);
        }

        if (difference_list.includes(DifferencesReferential.DECREASE_APY)) {
            return this.generateDecreaseApy(data);
        }

        return null;
    }


    /**
     * @param {Object} data
     * @returns {String}
     */
    generateNewProject(data) {
        const {
            new_project,
        } = data;

        const {
            asset,
            config,
            duration,
        } = new_project;

        const {
            annualInterestRate,
        } = config;

        const annual_interest_rate = Number(annualInterestRate) * 100;
        return `New #cryptocurrency #staking available on #Binance ! Now you can stak #${asset} tokens. Duration ${duration} days, annual interest rate ${annual_interest_rate.toFixed(2)}%`;
    }


    /**
     * @param {Object} data
     * @returns {String}
     */
    generateNoMoreProject(data) {
        const {
            old_project,
        } = data;

        const {
            asset,
        } = old_project;

        return `#${asset} is not proposed anymore on #Binance #staking`;
    }


    /**
     * @param {Object} data
     * @returns {String}
     */
    generateSoldOutTrue(data) {
        const {
            new_project,
        } = data;

        const {
            asset,
            duration,
        } = new_project;

        return `#${asset} cryptocurrency - ${duration} days staking was sold out on #Binance.`;
    }


    /**
     * @param {Object} data
     * @returns {String}
     */
    generateSoldOutFalse(data) {
        const {
            new_project,
        } = data;

        const {
            asset,
            duration,
            config: new_config,
        } = new_project;
        const {
            annualInterestRate: new_rate,
        } = new_config;
        const rate = (Number(new_rate) * 100).toFixed(2);
        const {
            date,
            day_of_week,
            time,
        } = this.getCurrentDate();

        return `Good news! Today, ${day_of_week} ${date} at ${time}, #staking of #${asset} #cryptocurrency for ${duration} days is available again on #Binance. Current APY rate ${rate}%`;
    }

    /**
     * @param {Object} data
     * @returns {String}
     */
    generateIncreaseApy(data) {
        const {
            new_project,
            old_project,
        } = data;

        const {
            asset,
            duration,
            config: new_config,
        } = new_project;

        const {
            config: old_config,
        } = old_project;

        const {
            annualInterestRate: old_rate,
        } = old_config;

        const {
            annualInterestRate: new_rate,
        } = new_config;

        const increase = (Number(new_rate) - Number(old_rate)) * 100;
        return `Annual interest rate increased of +${increase.toFixed(2)}% on #${asset} cryptocurrency staking for ${duration} days on #Binance.`;
    }


    /**
     * @param {Object} data
     * @returns {String}
     */
    generateDecreaseApy(data) {
        const {
            new_project,
            old_project,
        } = data;

        const {
            asset,
            duration,
            config: new_config,
        } = new_project;

        const {
            config: old_config,
        } = old_project;

        const {
            annualInterestRate: old_rate,
        } = old_config;

        const {
            annualInterestRate: new_rate,
        } = new_config;

        const increase = (Number(old_rate) - Number(new_rate)) * 100;
        return `Annual interest rate decreased of -${increase.toFixed(2)}% on #${asset} cryptocurrency staking for ${duration} days on #Binance.`;
    }

    /**
     * @return {Object}
     */
    getCurrentDate() {
        const current_moment_date = moment();
        const year = moment(current_moment_date).format('YYYY'); // May 16th 2021, 2:45:08 am
        const date = moment(current_moment_date).format('MMMM Do'); // May 16th 2021, 2:45:08 am
        const time = moment(current_moment_date).format('HH:mm'); // May 16th 2021, 2:45:08 am
        const day_of_week = moment(current_moment_date).format('dddd'); // day of week
        return {
            date,
            day_of_week,
            time,
            year,
        };
    }

}

module.exports = {
    TweetTextGeneratorService,
};
