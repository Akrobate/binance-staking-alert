'use strict';

class Logger {

    /**
     * @param {String|Object} params
     * @returns {void}
     */
    log(...message) {
        console.log(...message);
    }

}

module.exports = {
    Logger,
    logger: new Logger(),
};
