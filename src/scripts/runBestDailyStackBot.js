/* istanbul ignore file */

'use strict';

const {
    BestDailyStakBot,
} = require('../bots/BestDailyStakBot');

(async () => {

    const bot = BestDailyStakBot.getInstance();
    const metadata = await bot.updateCoinListMetadata();

    console.log(Object.keys(metadata));
    bot.generateDataAggregation();

})();
