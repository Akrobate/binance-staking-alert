/* istanbul ignore file */

'use strict';

const {
    SurveyStakingBot,
} = require('../bots/SurveyStakingBot');

(async () => {
    const survey_staking_bot = SurveyStakingBot.getInstance();
    await survey_staking_bot.run();
})();
