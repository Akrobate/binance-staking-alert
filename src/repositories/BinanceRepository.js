'use strict';

const axios = require('axios');

/**
 * Conncetor to interract with the Binance API
 * https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md#general-api-information
 *
 * https://github.com/binance/binance-spot-api-docs/blob/master/rest-api.md#current-average-price
 */
class BinanceRepository {

    /**
     * @return {BinanceRepository}
     */
    static getInstance() {
        if (BinanceRepository.instance === null) {
            BinanceRepository.instance = new BinanceRepository();
        }
        return BinanceRepository.instance;
    }

    /**
     * @return {BinanceRepository}
     */
    constructor() {
        this.base_api_url = 'https://api.binance.com/';
    }

    /**
     * Returns the current price
     * @param {String} symbol
     * @returns {Object}
     *
     * @example
     * getLatestPrice('ADAEUR')
     * {
     *   symbol: 'ADAEUR',
     *   price: '1.07795000',
     *   }
     */
    getLatestPrice(symbol) {
        return axios
            .get(`${this.base_api_url}api/v3/ticker/price`,
                {
                    params: {
                        symbol,
                    },
                })
            .then((response) => response.data);
    }


    /**
     *
     * @param {Object} params
     * @param {String} params.symbol required
     * @param {String} params.interval required
     * @param {Number} params.startTime
     * @param {Number} params.endTime
     * @param {Number} params.limit Default 500; max 1000.
     * @returns {Object}
     *
     *   [
     *    1499040000000,      // Open time
     *    "0.01634790",       // Open
     *    "0.80000000",       // High
     *    "0.01575800",       // Low
     *    "0.01577100",       // Close
     *    "148976.11427815",  // Volume
     *    1499644799999,      // Close time
     *    "2434.19055334",    // Quote asset volume
     *    308,                // Number of trades
     *    "1756.87402397",    // Taker buy base asset volume
     *    "28.46694368",      // Taker buy quote asset volume
     *    "17928899.62484339" // Ignore.
     *  ]
     */
    getCandlestickData(params) {
        return axios
            .get(`${this.base_api_url}api/v3/klines`,
                {
                    params,
                }
            )
            .then((response) => response.data);
    }

}

BinanceRepository.instance = null;

module.exports = {
    BinanceRepository,
};
