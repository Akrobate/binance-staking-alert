'use strict';

const fs = require('fs').promises;

class JsonFileRepository {


    /**
     * @returns {String}
     */
    static get DATA_FOLDER() {
        return `${__dirname}/../../data/`;
    }


    /**
     * @returns {String}
     */
    static get FILENAME() {
        return 'last_stacking_response.json';
    }

    /**
     * @static
     * @returns {JsonFileRepository}
     */
    static getInstance() {
        if (JsonFileRepository.instance) {
            return JsonFileRepository.instance;
        }
        JsonFileRepository.instance = new JsonFileRepository();
        return JsonFileRepository.instance;
    }


    /**
     * @return {Object}
     */
    async getData() {
        const string_data = await fs.readFile(
            `${JsonFileRepository.DATA_FOLDER}${JsonFileRepository.FILENAME}`
        );

        return JSON.parse(string_data);
    }


    /**
     * @param {Object} data
     * @return {Promise<Object>}
     */
    saveData(data) {
        const string_data = JSON.stringify(data);
        return fs.writeFile(
            `${JsonFileRepository.DATA_FOLDER}${JsonFileRepository.FILENAME}`,
            string_data
        );
    }
}

module.exports = {
    JsonFileRepository,
};
