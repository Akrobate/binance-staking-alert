'use strict';

const Twitter = require('twitter-lite');

const {
    configuration,
} = require('../services/ConfigurationService');


class TwitterRepository {


    /**
     * @static
     * @returns {TwitterRepository}
     */
    static getInstance() {
        if (TwitterRepository.instance) {
            return TwitterRepository.instance;
        }
        TwitterRepository.instance = new TwitterRepository();
        return TwitterRepository.instance;
    }

    /**
     * @returns {TwitterRepository}
     */
    constructor() {
        this.client = new Twitter(configuration.twitter);
    }


    /**
     * @param {String} message
     * @return {Promise<Object>}
     */
    tweet(message) {
        return this.client
            .post(
                'statuses/update',
                {
                    status: message,
                }
            );
    }

}

module.exports = {
    TwitterRepository,
};
