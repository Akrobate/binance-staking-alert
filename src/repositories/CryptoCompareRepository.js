'use strict';

const axios = require('axios');

class CryptoCompareRepository {

    /**
     * @return {CryptoCompareRepository}
     */
    static getInstance() {
        if (CryptoCompareRepository.instance === null) {
            CryptoCompareRepository.instance = new CryptoCompareRepository();
        }
        return CryptoCompareRepository.instance;
    }

    /**
     * @returns {CryptoCompareRepository}
     */
    constructor() {
        this.cryptocompare_min_url = 'https://min-api.cryptocompare.com/data/';
    }

    /**
     * @returns {Promise<Object>}
     */
    async getCoinList() {
        const response = await axios.get(`${this.cryptocompare_min_url}all/coinlist`);
        return response.data.Data;
    }

}

CryptoCompareRepository.instance = null;

module.exports = {
    CryptoCompareRepository,
};
