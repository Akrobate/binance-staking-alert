'use strict';

const {
    BinanceStackingRepository,
} = require('./BinanceStackingRepository');

const {
    JsonFileRepository,
} = require('./JsonFileRepository');

const {
    TwitterRepository,
} = require('./TwitterRepository');

const {
    CryptoCompareRepository,
} = require('./CryptoCompareRepository');

const {
    BinanceRepository,
} = require('./BinanceRepository');

module.exports = {
    BinanceRepository,
    BinanceStackingRepository,
    CryptoCompareRepository,
    JsonFileRepository,
    TwitterRepository,
};
