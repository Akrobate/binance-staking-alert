/* eslint-disable multiline-comment-style */

'use strict';

const axios = require('axios');

/**

 {
    asset: 'DOT',
    annualInterestRate: '0.18470000',
    minPurchaseAmount: '1.00000000',
    redeemPeriod: '1',
    products: [],
    projects: [ [Object], [Object], [Object] ]
   }


     {
    id: '512',
    projectId: 'Dot*90',
    asset: 'DOT',
    upLimit: '300000.00000000',
    purchased: '300000.00000000',
    endTime: '1698681600000',
    issueStartTime: '1603285200000',
    issueEndTime: '1667145600000',
    duration: '90',
    expectRedeemDate: '1628380800000',
    interestPerUnit: '0.04554270',
    withWhiteList: false,
    display: true,
    displayPriority: '64',
    status: 'PURCHASING',
    config: {
      id: '749',
      annualInterestRate: '0.18470000',
      dailyInterestRate: '0.00050603',
      extraInterestAsset: null,
      extraAnnualInterestRate: null,
      extraDailyInterestRate: null,
      minPurchaseAmount: '1.00000000',
      maxPurchaseAmountPerUser: '5000.00000000',
      chainProcessPeriod: '1',
      redeemPeriod: '1',
      payInterestPeriod: '1'
    },
    sellOut: true,
    createTimestamp: '1603278114000'
  }

 */

class BinanceStackingRepository {

    /* istanbul ignore next */
    /**
     * @static
     * @returns {BinanceStackingRepository}
     */
    static getInstance() {
        if (BinanceStackingRepository.instance) {
            return BinanceStackingRepository.instance;
        }
        BinanceStackingRepository.instance = new BinanceStackingRepository();
        return BinanceStackingRepository.instance;
    }


    /**
     * @returns {Number}
     */
    static get MAX_RESULTS() {
        return 200;
    }


    /**
     * @return {Object}
     */
    async getStakingProducts() {

        const response = await axios.get(
            'https://www.binance.com/bapi/earn/v1/friendly/pos/union',
            {
                params: {
                    pageSize: BinanceStackingRepository.MAX_RESULTS,
                    pageIndex: 1,
                    status: 'ALL',
                },
            }
        );

        const {
            data,
        } = response;
        return data;
    }
}

module.exports = {
    BinanceStackingRepository,
};
