# binance-staking-alert

## BestDailyStakBot

This bot will try to find the current best staking proposition. It takes account of various inputs to make decision:

* Current staking APY
* Staking availability
* Currency market capitalization
* Volatility score of the currency


## SurveyStakingBot

This bot will just survey the availability of staking products


## Installation

```bash
npm install
```