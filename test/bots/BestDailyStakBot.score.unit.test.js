'use strict';

const {
    mock,
} = require('sinon');
const {
    BestDailyStakBot,
} = require('../../src/bots');

const fs = require('fs').promises;

const {
    BinanceStackingRepository,
    JsonFileRepository,
    TwitterRepository,
    BinanceRepository,
} = require('../../src/repositories');

const {
    logger,
} = require('../../src/logger');
const {
    expect,
} = require('chai');


describe('BestDailyStakBot', () => {

    let best_daily_stak_bot = null;
    const mocks = {};

    beforeEach(() => {
        best_daily_stak_bot = BestDailyStakBot.getInstance();
        mocks.binance_stacking_repository = mock(BinanceStackingRepository.getInstance());
        mocks.binance_repository = mock(BinanceRepository.getInstance());
        mocks.json_file_repository = mock(JsonFileRepository.getInstance());
        mocks.best_daily_stak_bot = mock(best_daily_stak_bot);
        mocks.twitter_repository = mock(TwitterRepository.getInstance());
        mocks.logger = mock(logger);
        mocks.console = mock(console);
    });

    afterEach(() => {
        mocks.binance_stacking_repository.restore();
        mocks.json_file_repository.restore();
        mocks.best_daily_stak_bot.restore();
        mocks.twitter_repository.restore();
        mocks.logger.restore();
        mocks.console.restore();
    });

    it('generateDataAggregation', async () => {

        const mock_read_data = await fs.readFile('./test/seeds/seed_candlestick_ADAUSDT_1h.json');
        mocks.binance_repository.expects('getCandlestickData')
            .once()
            .returns(Promise.resolve(JSON.parse(mock_read_data)));
        const currency = 'ADA';
        const staking_duration_days = 90;
        const variability_score = await best_daily_stak_bot
            .getVariabiltyScoreForCurrency(currency, staking_duration_days);
        expect(variability_score.toFixed(4)).to.equal('0.1061');
    });

});
