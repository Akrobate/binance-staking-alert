'use strict';

const {
    mock,
} = require('sinon');
const fs = require('fs');
const {
    BestDailyStakBot,
} = require('../../src/bots');

const {
    BinanceStackingRepository,
    BinanceRepository,
    JsonFileRepository,
    TwitterRepository,
    CryptoCompareRepository,
} = require('../../src/repositories');

const {
    logger,
} = require('../../src/logger');
const {
    expect,
} = require('chai');


describe('BestDailyStakBot', () => {

    let best_daily_stak_bot = null;
    const mocks = {};

    beforeEach(() => {
        best_daily_stak_bot = BestDailyStakBot.getInstance();
        mocks.binance_staking_repository = mock(BinanceStackingRepository.getInstance());
        mocks.binance_repository = mock(BinanceRepository.getInstance());
        mocks.json_file_repository = mock(JsonFileRepository.getInstance());
        mocks.best_daily_stak_bot = mock(best_daily_stak_bot);
        mocks.twitter_repository = mock(TwitterRepository.getInstance());
        mocks.crypto_compare_repository = mock(CryptoCompareRepository.getInstance());
        mocks.logger = mock(logger);
        mocks.console = mock(console);
    });

    afterEach(() => {
        mocks.binance_staking_repository.restore();
        mocks.json_file_repository.restore();
        mocks.best_daily_stak_bot.restore();
        mocks.twitter_repository.restore();
        mocks.binance_repository.restore();
        mocks.crypto_compare_repository.restore();
        mocks.logger.restore();
        mocks.console.restore();
    });

    it('SurveyStakingBot run / stop', async () => {
        mocks.best_daily_stak_bot
            .expects('sleepAsync')
            .returns(Promise.resolve({}));
        mocks.best_daily_stak_bot.expects('process')
            .returns(
                Promise.resolve({}).then(() => best_daily_stak_bot.stop())
            );
        await best_daily_stak_bot.run();
    });

    it('sleepAsync', async () => {
        await best_daily_stak_bot.sleepAsync(1);
    });

    it('generateDataAggregation', async () => {

        // require('fs').writeFileSync('./test/seeds/getCoinList_seed.json', JSON.stringify(coinlist_full_metadata));

        mocks.crypto_compare_repository
            .expects('getCoinList')
            .returns(Promise.resolve(
                JSON.parse(fs.readFileSync(`${__dirname}/../seeds/getCoinList_seed.json`))
            ));

        mocks.binance_repository
            .expects('getLatestPrice')
            .returns(Promise.resolve(
                JSON.parse(fs.readFileSync(`${__dirname}/../seeds/getLatestPrice_seed.json`))
            ));

        mocks.binance_staking_repository
            .expects('getStakingProducts')
            .returns(Promise.resolve(
                JSON.parse(fs.readFileSync(`${__dirname}/../seeds/getStakingProducts_seed.json`))
            ));

        await best_daily_stak_bot.initData();

        best_daily_stak_bot.generateDataAggregation();

        const best_project_list = await best_daily_stak_bot
            .generateBestProjectListFromAggregatedFullData();

        expect(best_project_list).to.be.an('Array');
        expect(best_project_list.length).to.be.gte(1);
        const [
            first,
        ] = best_project_list;
        expect(first).to.be.an('Object');
        expect(first).to.have.property('capitalization');
        expect(first).to.have.property('duration');
        expect(first).to.have.property('annual_interest_rate');
    });

});
