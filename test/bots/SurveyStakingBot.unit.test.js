'use strict';

const {
    mock,
} = require('sinon');
const fs = require('fs');
const {
    SurveyStakingBot,
} = require('../../src/bots/SurveyStakingBot');

const {
    BinanceStackingRepository,
    JsonFileRepository,
    TwitterRepository,
} = require('../../src/repositories');

const {
    logger,
} = require('../../src/logger');
const {
    expect,
} = require('chai');


describe('SurveyStakingBot', () => {

    let survey_staking_bot = null;
    const mocks = {};

    beforeEach(() => {
        survey_staking_bot = SurveyStakingBot.getInstance();
        mocks.binance_stacking_repository = mock(BinanceStackingRepository.getInstance());
        mocks.json_file_repository = mock(JsonFileRepository.getInstance());
        mocks.survey_staking_bot = mock(survey_staking_bot);
        mocks.twitter_repository = mock(TwitterRepository.getInstance());
        mocks.logger = mock(logger);
        mocks.console = mock(console);
    });

    afterEach(() => {
        mocks.binance_stacking_repository.restore();
        mocks.json_file_repository.restore();
        mocks.survey_staking_bot.restore();
        mocks.twitter_repository.restore();
        mocks.logger.restore();
        mocks.console.restore();
    });

    it('SurveyStakingBot run / stop', async () => {
        mocks.survey_staking_bot
            .expects('sleepAsync')
            .returns(Promise.resolve({}));
        mocks.survey_staking_bot.expects('process')
            .returns(
                Promise.resolve({}).then(() => survey_staking_bot.stop())
            );
        await survey_staking_bot.run();
    });

    it('SurveyStakingBot process', async () => {
        mocks.survey_staking_bot
            .expects('tweetStatusList')
            .returns(Promise.resolve({}));

        mocks.binance_stacking_repository
            .expects('getStakingProducts')
            .returns(Promise.resolve(
                JSON.parse(fs.readFileSync(`${__dirname}/../seeds/binance_real_response.json`))
            ));

        mocks.json_file_repository
            .expects('getData')
            .returns(Promise.resolve(
                JSON.parse(fs.readFileSync(`${__dirname}/../seeds/last_stacking_response.json`))
            ));

        mocks.json_file_repository
            .expects('saveData')
            .returns(Promise.resolve());

        await survey_staking_bot.process();
        mocks.json_file_repository.verify();
        mocks.binance_stacking_repository.verify();
        mocks.survey_staking_bot.verify();
    });

    it('getAndUpdateData', async () => {

        mocks.binance_stacking_repository
            .expects('getStakingProducts')
            .returns(Promise.resolve(
                JSON.parse(fs.readFileSync(`${__dirname}/../seeds/binance_real_response.json`))
            ));

        mocks.json_file_repository
            .expects('getData')
            .returns(Promise.resolve(
                JSON.parse(fs.readFileSync(`${__dirname}/../seeds/last_stacking_response.json`))
            ));

        mocks.json_file_repository
            .expects('saveData')
            .returns(Promise.resolve());

        const data = await survey_staking_bot.getAndUpdateData();
        mocks.json_file_repository.verify();
        mocks.binance_stacking_repository.verify();
        expect(data).to.have.property('fetched_data');
        expect(data).to.have.property('saved_data');
        expect(data.fetched_data).to.have.property('total', 56);
        expect(data.saved_data).to.have.property('total', 56);
    });


    it('SurveyStakingBot run / stop error management', async () => {
        mocks.survey_staking_bot
            .expects('sleepAsync')
            .returns(Promise.resolve({}));
        mocks.survey_staking_bot.expects('process')
            .returns(
                Promise.resolve({}).then(() => {
                    survey_staking_bot.stop();
                    throw new Error('Process problem');
                })
            );
        mocks.console
            .expects('log')
            .returns(null);
        await survey_staking_bot.run();
    });

    it('tweetStatusList', async () => {
        const tweet = {
            message: 'test',
        };

        const tweet_list = [
            tweet,
        ];

        mocks.twitter_repository
            .expects('tweet')
            .withArgs(tweet)
            .returns(Promise.resolve({}));

        mocks.survey_staking_bot
            .expects('sleepAsync')
            .returns(Promise.resolve({}));

        await survey_staking_bot.tweetStatusList(tweet_list);
        mocks.survey_staking_bot.verify();
        mocks.twitter_repository.verify();

    });

    it('tweetStatusList error case', async () => {
        const tweet = {
            message: 'test',
        };

        const tweet_list = [
            tweet,
        ];

        mocks.twitter_repository
            .expects('tweet')
            .withArgs(tweet)
            .throws(new Error('Random error'));

        mocks.survey_staking_bot
            .expects('sleepAsync')
            .returns(Promise.resolve({}));

        mocks.logger.expects('log')
            .returns(null);

        mocks.logger.expects('log')
            .returns(null);

        mocks.logger.expects('log')
            .returns(null);

        await survey_staking_bot.tweetStatusList(tweet_list);
        mocks.survey_staking_bot.verify();
        mocks.twitter_repository.verify();

    });

    it('sleepAsync', async () => {
        await survey_staking_bot.sleepAsync(1);
    });
});
