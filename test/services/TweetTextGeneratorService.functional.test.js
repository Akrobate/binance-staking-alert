'use strict';

const {
    TweetTextGeneratorService,
} = require('../../src/services/TweetTextGeneratorService');

const moment = require('moment');


const {
    expect,
} = require('chai');

const {
    useFakeTimers,
} = require('sinon');

const {
    new_project_detection: new_project_detection_seed,
    no_more_project_detection: no_more_project_detection_seed,
    sold_out_true_detection: sold_out_true_detection_seed,
    sold_out_false_detection: sold_out_false_detection_seed,
    decrease_apy_detection: decrease_apy_detection_seed,
    increase_apy_detection: increase_apy_detection_seed,
} = require('../seeds/detections_types.json');

describe('TweetTextGeneratorService', () => {

    const clocks = {};
    let tweet_text_generator_service = null;
    const date_iso_string = '2021-05-21T14:30:00.000Z';

    beforeEach(() => {
        tweet_text_generator_service = TweetTextGeneratorService.getInstance();
        clocks.now = useFakeTimers(moment(date_iso_string).valueOf());
    });

    afterEach(() => {
        tweet_text_generator_service = null;
        clocks.now.restore();
    });

    it('new_project', () => {
        const text = tweet_text_generator_service.generate(new_project_detection_seed);
        expect(text).to.equal('New #cryptocurrency #staking available on #Binance ! Now you can stak #TVK tokens. Duration 15 days, annual interest rate 47.67%');
    });

    it('no_more_project', () => {
        const text = tweet_text_generator_service.generate(no_more_project_detection_seed);
        expect(text).to.equal('#LTO is not proposed anymore on #Binance #staking');
    });

    it('sold_out_true', () => {
        const text = tweet_text_generator_service.generate(sold_out_true_detection_seed);
        expect(text).to.equal('#TVK cryptocurrency - 90 days staking was sold out on #Binance.');
    });

    it('sold_out_false', () => {
        const text = tweet_text_generator_service.generate(sold_out_false_detection_seed);
        expect(text.length).to.be.below(160);
        const should_be_time = moment(date_iso_string).format('HH:mm');
        expect(text).to.equal(`Good news! Today, Friday May 21st at ${should_be_time}, #staking of #XTZ #cryptocurrency for 90 days is available again on #Binance. Current APY rate 11.79%`);
    });

    it('increase_apy', () => {
        const text = tweet_text_generator_service.generate(increase_apy_detection_seed);
        expect(text).to.equal('Annual interest rate increased of +5.00% on #AVAX cryptocurrency staking for 90 days on #Binance.');
    });

    it('decrease_apy', () => {
        const text = tweet_text_generator_service.generate(decrease_apy_detection_seed);
        expect(text).to.equal('Annual interest rate decreased of -5.00% on #AVAX cryptocurrency staking for 30 days on #Binance.');
    });

    it('Unkwnown case', () => {
        const text = tweet_text_generator_service.generate({
            difference_list: [],
        });
        expect(text).to.equal(null);
    });

    it('getCurrentDate', () => {
        const date_object = tweet_text_generator_service.getCurrentDate();

        const year = moment(date_iso_string).format('YYYY');
        const date = moment(date_iso_string).format('MMMM Do');
        const time = moment(date_iso_string).format('HH:mm');
        const day_of_week = moment(date_iso_string).format('dddd');

        expect(date_object).to.deep.equal({
            date, // 'May 21st',
            day_of_week, // 'Friday',
            time, // '16:30',
            year, // '2021',
        });
    });

});
