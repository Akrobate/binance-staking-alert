'use strict';

const {
    expect,
} = require('chai');

const fs = require('fs');

const {
    mock,
} = require('sinon');

const {
    ConfigurationService,
} = require('../../src/services');


describe('ConfigurationService', () => {

    const mocks = {};
    const configuration_service = ConfigurationService.getInstance();

    beforeEach(() => {
        mocks.configuration_service = mock(configuration_service);
        mocks.fs = mock(fs);
    });

    afterEach(() => {
        mocks.fs.restore();
        mocks.configuration_service.restore();
    });

    it('lazyLoadConfiguration', () => {
        const configuration = configuration_service.lazyLoadConfiguration();
        expect(configuration).to.have.property('twitter');
        expect(configuration.twitter).to.have.property('consumer_key');
    });

    it('loadConfiguration forced configuration case', () => {
        mocks.fs
            .expects('existsSync')
            .returns(true);
        mocks.configuration_service
            .expects('requireFile')
            .returns({
                configuration: {
                    twitter: {
                        consumer_key: 'test',
                    },
                },
            });
        const configuration = configuration_service.loadConfiguration();
        expect(configuration).to.have.property('twitter');
        expect(configuration.twitter).to.have.property('consumer_key', 'test');
    });

    it('loadConfiguration forced example case', () => {
        mocks.fs
            .expects('existsSync')
            .returns(false);

        const configuration = configuration_service.loadConfiguration();
        expect(configuration).to.have.property('twitter');
        expect(configuration.twitter).to.have.property('consumer_key');
    });

});

