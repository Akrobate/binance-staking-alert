'use strict';

const fs = require('fs');

const {
    DifferencesCalculatorService,
} = require('../../src/services/DifferencesCalculatorService');

const {
    expect,
} = require('chai');

// Seed information
// last_stacking_response Tvk*15 was removed
// binance_real_response Lto*7 was removed
// binance_real_response Xtz*90 was marked as sellOut: false
// binance_real_response Avax*90 annualInterestRate +0.1 %
// binance_real_response Avax*30 annualInterestRate -0.05 %

describe('DifferencesCalculatorService', () => {

    let saved_data = null;
    let fetched_data = null;

    beforeEach(() => {
        fetched_data = JSON.parse(fs.readFileSync(`${__dirname}/../seeds/binance_real_response.json`));
        saved_data = JSON.parse(fs.readFileSync(`${__dirname}/../seeds/last_stacking_response.json`));
    });

    it('Should be able to build all projects list', () => {

        const differences_calculator_service = DifferencesCalculatorService.getInstance();

        const one_level_saved_data = differences_calculator_service
            .formatOneLevelData(saved_data.data);
        const one_level_fetched_data = differences_calculator_service
            .formatOneLevelData(fetched_data.data);

        const response = differences_calculator_service
            .generateProjectsList(one_level_saved_data, one_level_fetched_data);

        expect(response.length).to.equal(157);
        expect(one_level_saved_data.length).to.equal(156);
        expect(one_level_fetched_data.length).to.equal(156);

    });

    it('Should be able to correctly detect differences', async () => {
        const differences_calculator_service = DifferencesCalculatorService.getInstance();

        differences_calculator_service.setFetchedData(fetched_data);
        differences_calculator_service.setSavedData(saved_data);

        const differences = await differences_calculator_service
            .process();

        const new_project_detection = differences
            .find((element) => element.difference_list.includes('new_project'));
        expect(new_project_detection).to.not.equal(undefined);
        expect(new_project_detection.old_project).to.equal(undefined);
        expect(new_project_detection.new_project).to.be.an('Object');

        const no_more_project_detection = differences
            .find((element) => element.difference_list.includes('no_more_project'));
        expect(no_more_project_detection).to.not.equal(undefined);
        expect(no_more_project_detection.new_project).to.equal(undefined);
        expect(no_more_project_detection.old_project).to.be.an('Object');

        const sold_out_true_detection = differences
            .find((element) => element.difference_list.includes('sold_out_true'));
        expect(sold_out_true_detection.new_project.sellOut).to.equal(true);
        expect(sold_out_true_detection.old_project.sellOut).to.equal(false);

        const sold_out_false_detection = differences
            .find((element) => element.difference_list.includes('sold_out_false'));
        expect(sold_out_false_detection.new_project.sellOut).to.equal(false);
        expect(sold_out_false_detection.old_project.sellOut).to.equal(true);

        const increase_apy_detection = differences
            .find((element) => element.difference_list.includes('increase_apy'));
        expect(
            Number(increase_apy_detection.old_project.config.annualInterestRate)
        ).be.below(
            Number(increase_apy_detection.new_project.config.annualInterestRate)
        );

        const decrease_apy_detection = differences
            .find((element) => element.difference_list.includes('decrease_apy'));
        expect(
            Number(decrease_apy_detection.old_project.config.annualInterestRate)
        ).be.above(
            Number(decrease_apy_detection.new_project.config.annualInterestRate)
        );

    });


    it('Should be able to generateProjectsList', () => {
        const differences_calculator_service = DifferencesCalculatorService.getInstance();
        const old_data = [
            {
                projectId: 1,
            },
            {
                projectId: 1,
            },
        ];
        const new_data = [
            {
                projectId: 2,
            },
            {
                projectId: 2,
            },
        ];
        const project_list = differences_calculator_service
            .generateProjectsList(old_data, new_data);
        expect(project_list).to.deep.equal([1, 2]);
    });

});
