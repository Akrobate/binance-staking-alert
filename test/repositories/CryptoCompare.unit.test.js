/* eslint-disable sort-keys */

'use strict';

const {
    mock,
} = require('sinon');
const {
    expect,
} = require('chai');
const axios = require('axios');
const {
    CryptoCompareRepository,
} = require('../../src/repositories');


const mocks = {};
let crypto_compare = null;

describe('CryptoCompare', () => {

    beforeEach(() => {
        mocks.axios = mock(axios);
        crypto_compare = CryptoCompareRepository.getInstance();
    });

    afterEach(() => {
        mocks.axios.restore();
        crypto_compare = null;
    });

    it('Should be able to get coinlist', async () => {

        mocks.axios.expects('get')
            .withArgs('https://min-api.cryptocompare.com/data/all/coinlist')
            .once()
            .returns(Promise.resolve(
                {
                    data: {
                        Data: {
                            POLAR: {
                                Id: '940224',
                                Url: '/coins/polar/overview',
                                ImageUrl: '/media/37746812/polar.png',
                                ContentCreatedOn: 1617189485,
                                Name: 'POLAR',
                                Symbol: 'POLAR',
                                CoinName: 'Polaris',
                                FullName: 'Polaris (POLAR)',
                                Description: 'Polaris is a farming protocol and launchpad platform designed to solve several issues with liquidity mining (yield farming) on Binance Smart Chain.',
                                AssetTokenStatus: 'N/A',
                                Algorithm: 'N/A',
                                ProofType: 'N/A',
                                SortOrder: '6842',
                                Sponsored: false,
                                Taxonomy: [Object],
                                Rating: [Object],
                                IsTrading: false,
                                TotalCoinsMined: 94870.01072727273,
                                BlockNumber: 0,
                                NetHashesPerSecond: 0,
                                BlockReward: 0,
                                BlockTime: 0,
                                AssetLaunchDate: '2021-03-11',
                                MaxSupply: -1,
                                MktCapPenalty: 0,
                                PlatformType: 'token',
                                DecimalPoints: 18,
                                BuiltOn: 'BNB',
                                SmartContractAddress: '0x3a5325f0e5ee4da06a285e988f052d4e45aa64b4',
                            },
                        },
                        BaseImageUrl: 'https://www.cryptocompare.com',
                        BaseLinkUrl: 'https://www.cryptocompare.com',
                        RateLimit: {},
                        HasWarning: false,
                        Type: 100,
                        Response: 'Success',
                        Message: 'Coin list succesfully returned!',
                    },
                }
            ));

        const data = await crypto_compare.getCoinList();
        expect(data).to.haveOwnProperty('POLAR');
        expect(data.POLAR).to.haveOwnProperty('Name');
        expect(data.POLAR).to.haveOwnProperty('Symbol');
    });

});
