'use strict';

const {
    JsonFileRepository,
} = require('../../src/repositories');

const fs = require('fs').promises;

const {
    expect,
} = require('chai');

const {
    mock,
} = require('sinon');

describe('JsonFileRepository', () => {

    const mocks = {};
    let json_file_repository = null;

    beforeEach(() => {
        json_file_repository = JsonFileRepository.getInstance();
        mocks.fs = mock(fs);
    });

    afterEach(() => {
        mocks.fs.restore();
    });

    it('getData', async () => {
        mocks.fs.expects('readFile')
            .returns(Promise.resolve(JSON.stringify({
                data: [],
            })));

        const result = await json_file_repository.getData();
        expect(result).to.have.property('data');
        mocks.fs.verify();
    });

    it('saveData', async () => {
        const data = {
            data: [],
        };

        mocks.fs.expects('writeFile')
            .withArgs(
                `${JsonFileRepository.DATA_FOLDER}${JsonFileRepository.FILENAME}`,
                JSON.stringify(data)
            )
            .returns(Promise.resolve(JSON.stringify({
                data: [],
            })));

        const result = await json_file_repository.saveData(data);
        expect(result).to.equal(JSON.stringify(data));
        mocks.fs.verify();
    });

});
