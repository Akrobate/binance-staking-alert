'use strict';

const {
    BinanceStackingRepository,
} = require('../../src/repositories/BinanceStackingRepository');

const fs = require('fs');
const axios = require('axios');

const {
    expect,
} = require('chai');

const {
    mock,
} = require('sinon');

describe('BinanceStackingRepository', () => {

    const mocks = {};
    let binance_real_data = null;
    let binance_stacking_repository = null;

    beforeEach(() => {
        binance_real_data = JSON.parse(fs.readFileSync(`${__dirname}/../seeds/binance_real_response.json`));
        binance_stacking_repository = BinanceStackingRepository.getInstance();
        mocks.axios = mock(axios);
    });

    afterEach(() => {
        binance_real_data = null;
        mocks.axios.restore();
    });

    it('getStakingProducts', async () => {
        mocks.axios.expects('get')
            .withArgs(
                'https://www.binance.com/bapi/earn/v1/friendly/pos/union',
                {
                    params: {
                        pageSize: BinanceStackingRepository.MAX_RESULTS,
                        pageIndex: 1,
                        status: 'ALL',
                    },
                }
            )
            .returns(Promise.resolve({
                data: binance_real_data,
            }));

        const result = await binance_stacking_repository.getStakingProducts();
        expect(result).to.have.property('total', 56);
        expect(result).to.have.property('success', true);
    });

});
