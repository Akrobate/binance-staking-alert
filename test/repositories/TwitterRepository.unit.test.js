'use strict';

const {
    TwitterRepository,
} = require('../../src/repositories');

const {
    expect,
} = require('chai');

const {
    mock,
} = require('sinon');

describe('TwitterRepository', () => {

    const mocks = {};
    let twitter_repository = null;

    beforeEach(() => {
        twitter_repository = TwitterRepository.getInstance();
        mocks.client = mock(twitter_repository.client);
    });

    afterEach(() => {
        mocks.client.restore();
    });

    it('tweet', async () => {
        const message = 'Test tweet';
        mocks.client.expects('post')
            .withArgs(
                'statuses/update',
                {
                    status: message,
                }
            )
            .returns(Promise.resolve({
                response: 'ok',
            }));

        const result = await twitter_repository.tweet(message);
        expect(result).to.have.property('response', 'ok');
        mocks.client.verify();
    });

});
